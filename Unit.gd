extends Spatial
class_name Unit

#var _nickname:String = "Salchiche"
#var _gid:int = -1 # GAME ID

const SPEED = 4
const ROTATION_SPEED = 6

const ACCEL = 8
const DEACCEL = 13

var _path:PoolVector3Array
var _path_distance:float
var _speed:float

onready var _graph = get_node("../Environment/Grid")

func _ready() -> void:
	$RotationHelper/CharacterModel/Armature/AnimationPlayer.current_animation = "move.idle"

func get_path_distance() -> float:
	var d = 0
	for i in _path.size() - 1:
		d += _path[i].distance_to(_path[i + 1])
	return d

func _draw_path() -> void:
	var im = $Node/Draw
	im.clear()
	im.begin(Mesh.PRIMITIVE_LINE_STRIP)
	im.set_color(Color(0.25, 0.25, 0.25, 0.2))
	for p in _path:
		im.add_vertex(p + Vector3.UP * 0.25)
	im.end()
	im.begin(Mesh.PRIMITIVE_POINTS)
	im.set_color(Color.white)
	for p in _path:
		im.add_vertex(p + Vector3.UP * 0.25)
	im.end()

func move_to(position:Vector3) -> void:
	if not _graph._generation_finished:
		return
	if _path.size() != 0:
		_graph.cell_set_disabled_from_global_point(_path[_path.size() - 1], self, false)
	var path = _graph.get_global_point_path(global_transform.origin, position, 4)
	if path.size() > 1:
		_path = path
		_path_distance = get_path_distance()
		# REMOVE THE FIRST POINT
		if _path.size() > 1:
			_path.remove(0)
		_draw_path()
		_graph.cell_set_disabled_from_global_point(global_transform.origin, self, false)
		_graph.cell_set_disabled_from_global_point(_path[_path.size() - 1], self, true)

func _physics_process(delta: float) -> void:
	if not _graph._generation_finished:
		return
	if _path.size() > 0:
		# GET MOVE DIRECTION
		var topos = _path[0]
		var topos_dis = global_transform.origin.distance_to(topos) # this is 'cus in move_to() we do _path.remove(0)
		var move_dir = global_transform.origin.direction_to(topos)
		# ROTATE
		var rot_diff = 0
		var rot_dir = -move_dir
#		var SIZE = 2
#		if _path.size() > SIZE: # LOOK AHEAD
#			var pos = topos.linear_interpolate(_path[SIZE], 0.5)
#			rot_dir = -global_transform.origin.direction_to(pos)
		rot_dir.y = 0
		rot_dir = rot_dir.normalized()
		if rot_dir.length() > 0.001:
			var q_from = Quat($RotationHelper.global_transform.basis)
			var q_to = Quat(Transform().looking_at(rot_dir, Vector3.UP).basis)
			$RotationHelper.global_transform.basis = Basis(q_from.slerp(q_to, ROTATION_SPEED * delta))
			rot_diff = $RotationHelper.global_transform.basis.z.angle_to(-rot_dir) / PI
		# SET SPEED
		# https://gamedev.stackexchange.com/questions/73627/move-a-2d-point-to-a-target-first-accelerate-towards-the-target-then-decelerat
		if _path_distance + topos_dis > _speed * _speed / (2 * DEACCEL):
			var speed_rot_multi = 1.0 - rot_diff # * 0.85
			_speed = lerp(_speed, SPEED * speed_rot_multi, ACCEL * delta) # ACCELERATE WHEN FAR FROM TARGET
		else:
			_speed = lerp(_speed, 0, DEACCEL * delta) # DEACCELERATE WHEN NEAR TARGET
		# SET POSTION
		var step_dis = _speed  * delta
		var step = move_dir * step_dis
		global_transform.origin += step
		if topos_dis <= step_dis:
			_path.remove(0)
			_draw_path()
			_path_distance = get_path_distance()
			if _path.size() == 0:
				global_transform.origin = topos
				_graph.cell_set_disabled_from_global_point(global_transform.origin, self, true)