extends Area
class_name FDZGridWallDiagonal

const PTYPE_WALL_DIAGONAL = 2
var PLACEABLE_TYPE = PTYPE_WALL_DIAGONAL

var grid:FDZGrid = null
var cell_id:int

func unplace() -> void:
	if grid != null:
		grid.pathfiding_update()
		grid.cell_set_disabled(cell_id, self, false)
		grid.cell_erase_placeable(cell_id, self)
		grid.placeables_erase(self)
	queue_free()

func flip() -> void:
	global_transform.basis = global_transform.rotated(Vector3.UP, PI / 2).basis
	grid.pathfiding_update()

func place_from_global_point(grid:FDZGrid, global_point:Vector3) -> bool:
	if not grid.has_global_point(global_point):
		return false
	
	var cell_id = grid.get_id_from_global_point(global_point)
	var cell_position = grid.get_cell_global_point(cell_id)
	
	if grid.cell_get_disablers(cell_id).size() != 0:
		return false
	
	global_transform.origin = cell_position
	
	self.grid = grid
	self.cell_id = cell_id
	
	grid.cell_set_disabled(cell_id, self, true)
	grid.cell_push_placeable(cell_id, self)
	grid.placeables_push(self)
	
	grid.pathfiding_update()
	return true

func pathfiding_update() -> void:
	var euler_y = round(rad2deg(global_transform.basis.get_euler().y))
	if euler_y == 0 or euler_y == -180:
		grid.pathfinding_disconnect_points(cell_id - 1, cell_id + grid.SIZE)
		grid.pathfinding_disconnect_points(cell_id + 1, cell_id - grid.SIZE)
	else:
		grid.pathfinding_disconnect_points(cell_id - 1, cell_id - grid.SIZE)
		grid.pathfinding_disconnect_points(cell_id + 1, cell_id + grid.SIZE)