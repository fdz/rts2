extends Spatial

const ZOOM_SPEED = 3

export (float, 0.1, 1000) var camera_distance:float = 15
onready var cur_unit = get_node("../Unit")
onready var _grid = get_node("../Environment/Grid")
onready var _camera = $Camera

const _ORIENTATION_STEP = deg2rad(45)
var _orientation:Transform

onready var _cur_placeable_index:int = 0
onready var _placeables = [
	preload("res://FDZGridWall.tscn"),
	preload("res://FDZGridWallDiagonal.tscn"),
]

var building:bool

func _ready() -> void:
	_orientation = transform.basis
	self.camera_distance = camera_distance

func _physics_process(delta: float) -> void:
	if cur_unit == null:
		return
	global_transform.origin = global_transform.origin.linear_interpolate(cur_unit.global_transform.origin, delta * 4)

func _process(delta: float) -> void:
	if _grid.is_generating():
		return
	
	if Input.is_action_pressed("move_up"):
		camera_distance -= delta * ZOOM_SPEED
	if Input.is_action_pressed("move_down"):
		camera_distance += delta * ZOOM_SPEED
	
	camera_distance = clamp(camera_distance, 0.1, camera_distance)
	_camera.translation = Vector3(camera_distance, camera_distance * 2, camera_distance)

	if Input.is_action_just_pressed("move_right"):
		_orientation = _orientation.rotated(Vector3.UP, _ORIENTATION_STEP)
	if Input.is_action_just_pressed("move_left"):
		_orientation = _orientation.rotated(Vector3.UP, -_ORIENTATION_STEP)

	var q_from = Quat(transform.basis)
	var q_to = Quat(_orientation.basis)
	transform.basis = Basis(q_from.slerp(q_to, delta * 8))

	if Input.is_action_just_pressed("jump"):
		building = !building
	
	if building and Input.is_action_just_pressed("rts_next"):
		_cur_placeable_index += 1
		if _cur_placeable_index >= _placeables.size():
			_cur_placeable_index = 0
	
	$Label.text = "Unit Mode"
	if building:
		$Label.text = "Building Mode\n"
		if _cur_placeable_index == 0:
			$Label.text += "Placing Standard Wall"
		else:
			$Label.text += "Placing Diagonal Wall"
	
	if !building and Input.is_action_just_pressed("click_left"):
		var hit = doray(Phys.DEFAULT | Phys.GRIDS)
		if hit.size() > 0:
			if  hit.collider is Unit:
				cur_unit = hit.collider
			elif  cur_unit != null and hit.collider.collision_layer == Phys.GRIDS:
				cur_unit.move_to(hit.position)
			
	if building and Input.is_action_just_pressed("click_right"):
		var hit = doray(Phys.DEFAULT)
		if hit.size() > 0 and hit.collider and hit.collider.has_method("flip"):
			hit.collider.flip()
			
	if building and Input.is_action_just_pressed("click_left"):
		var hit = doray(Phys.DEFAULT | Phys.GRIDS)
		if hit.size() > 0:
			if Input.is_action_pressed("rts_delete"):
				if hit.collider and hit.collider.has_method("unplace"):
					hit.collider.unplace()
			else:
				var p = _placeables[_cur_placeable_index].instance()
				owner.add_child(p)
				if p.place_from_global_point(_grid, hit.position):
					print("placed " + str(p.name))
				else:
					p.free()
	
func doray(layer:int) -> Dictionary:
	var mouse_position = get_viewport().get_mouse_position()
	var from = _camera.project_ray_origin(mouse_position)
	var to = from + _camera.project_ray_normal(mouse_position) * 10000
	var hit = get_world().direct_space_state.intersect_ray(from, to, [], layer, true, true)
	return hit